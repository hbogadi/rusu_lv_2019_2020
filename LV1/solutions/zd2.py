try:
    broj = float(input("Unesite ocjenu: "))
except:
    print("Molim unesite broj")

if (broj >= 0.0 and broj <= 1.0):
    if (broj < 0.6):
        print("F")
    elif (broj >= 0.6 and broj < 0.7):
        print("D")
    elif (broj >= 0.7 and broj < 0.8):
        print("C")
    elif (broj >= 0.8 and broj < 0.9):
        print("B")
    elif (broj >= 0.9):
        print("A")
else:
    print("Broj nije u rasponu 0.0 i 1.0")




