#!/usr/bin/env python
# coding: utf-8

# In[86]:


import numpy as np
from sklearn import neural_network as nn
from sklearn import preprocessing as prep
from sklearn import metrics
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt


# In[87]:


#Funkcija 7.2.

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T


# In[88]:


dataLearn = non_func(500)
dataTest = non_func(200)
hiddenLayers = (20, 20, 10)

X = dataLearn[:, 0:1]
Y = dataLearn[:, 1]
YReal = dataLearn[:, 2]
XTest = dataTest[:, 0:1]
YTest = dataTest[:, 1]

mlp = nn.MLPRegressor(hiddenLayers, max_iter = 50000)


# In[89]:


mlp.fit(X = X, y = YReal)
predicted = mlp.predict(XTest)

predictedErr = mean_squared_error(YTest, predicted)
print(predictedErr)


# In[90]:


plt.figure(1)
plt.plot(XTest,predicted,'og',label='predicted')
plt.plot(XTest,YTest,'or',label='test')
plt.legend(loc = 4)


# In[ ]:




