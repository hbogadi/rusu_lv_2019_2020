arr = []

while(True):
    uin = input("Unesite broj ili 'Done': ")
    print(uin)
    if(uin != 'Done'):
        try:
            float(uin)
        except ValueError:
            try:
                int(uin)
            except ValueError:
                print("Input nije broj niti 'Done'!")
                continue
        arr.append(float(uin))
    else:
        print("Najveci uneseni broj je: ", max(arr))
        print("Najmanji uneseni broj je: ", min(arr))
        avg = sum(arr) / len(arr)
        print("Uneseno je ", len(arr), "brojeva.")
        print("Prosijek unesenih brojeva je: ", avg)
        break