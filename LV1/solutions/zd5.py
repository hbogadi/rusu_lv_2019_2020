fileName = input('Enter the file name: ')
values = []
try:
    file = open(fileName, 'r')
except:
    print('File cannot be opened:', fileName)
    exit()

for line in file:
        if(line.startswith("X-DSPAM-Confidence: ")):
            target = line
            target = target.replace("X-DSPAM-Confidence: ", '')
            values.append(float(target))
print (sum(values) / len(values))