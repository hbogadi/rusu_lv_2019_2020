#!/usr/bin/env python
# coding: utf-8

# In[66]:


import numpy as np
import matplotlib.pyplot as plt
#Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
#označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
#odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
#standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
#i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
#funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
#np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 

np.random.seed(37)

heightsm = []
heightsf = []

people = np.random.randint(low=0, high=2, size=1000)

for person in people:
    if(person == 1):
        heightsm.append(np.random.normal(loc=180, scale=7))
    elif(person == 0):
        heightsf.append(np.random.normal(loc=167, scale=7))

plt.hist(heightsm, bins=50, color=(0,0,1,0.5))
plt.hist(heightsf, bins=50, color=(1,0,0,0.5))

avgm = sum(heightsm)/len(heightsm)
avgf = sum(heightsf)/len(heightsf)

plt.axvline(avgm, color="blue")
plt.axvline(avgf, color="red")


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




