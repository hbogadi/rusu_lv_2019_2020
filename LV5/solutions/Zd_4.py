#!/usr/bin/env python
# coding: utf-8

# In[24]:


# -*- coding: utf-8 -*-
"""
Created on Sun Dec 02 12:08:00 2018

@author: Grbic
"""
import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

imageNew = mpimg.imread('example_grayscale.png')

#try:
#    face = sp.face(gray=True)
#except AttributeError:
#    from scipy import misc
#    face = misc.face(gray=True)
    
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew,  cmap='gray')

plt.figure(2)
plt.imshow(img_compressed,  cmap='gray')




# In[ ]:




